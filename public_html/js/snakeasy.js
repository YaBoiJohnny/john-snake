/*----------------------------------------------------------------------------
 * Variables
 * ---------------------------------------------------------------------------
 */

var snake;
var snakeLength;
var snakeSize;
var snakeDirection;

var food;

var Highscore = localStorage.getItem(HighscoreX);
var HighscoreX;


var context;
var screenWidth;
var screenHeight;

var gameState;
var gameOverMenu;
var playHud;
var scoreBoard;

var gameStartMenu;

var image;

var blopSound;

var deathSound;

var image2;


/*----------------------------------------------------------------------------
 * Executing Game Code
 * --------------------------------------------------------------------------- 
 */


gameInitialize();
snakeInitialize();
foodInitialize();
setInterval(gameLoop, 1000 / 15);


/*-----------------------------------------------------------------------------
 * Game Functions
 * ----------------------------------------------------------------------------
 */



function gameInitialize() {
    var canvas = document.getElementById("game-screen");
    context = canvas.getContext("2d");

    screenWidth = window.innerWidth;
    screenHeight = window.innerHeight;

    canvas.width = screenWidth;
    canvas.height = screenHeight;
    
     Highscore = document.getElementById("Highscore");

    document.addEventListener("keydown", keyboardHandler);
    
    gameOverMenu = document.getElementById("gameOver");
    centerMenuPosition(gameOverMenu);
    
    restartButton = document.getElementById("restartButton");
    restartButton.addEventListener("click", gameRestart);
    
    playHud = document.getElementById("playHUD");
    scoreboard = document.getElementById("scoreboard"); 
    
    
    
    startButton = document.getElementById("startButton");
    startButton.addEventListener("click", gameRestart);
    
    blopSound = new Audio("sounds/blop.mp3");
   blopSound.preload = "auto";
    
    deathSound = new Audio("sounds/death.mp3");
    deathSound.preload = "auto";
    
    image = document.getElementById("source");
    image2 = document.getElementById("source2");
    
    setState("PLAY");

}

function gameLoop() {
    gameDraw();
    drawScoreboard();
    
    if (gameState == "PLAY") {
        
        snakeUpdate();
        snakeDraw();
        foodDraw();
        HighScore();
        
    }
}

function gameDraw() {
    context.fillStyle = "rgb(0, 0, 0)";
    context.fillRect(0, 0, screenWidth, screenHeight);
}

function gameRestart() {
    snakeInitialize();
    foodInitialize();
    hideMenu(gameOverMenu);
    setState("PLAY");
}



/*-----------------------------------------------------------------------------
 * Snake Functions
 * ----------------------------------------------------------------------------
 */

function snakeInitialize() {
    snake = [];
    snakeLength = 1;
    snakeSize = 20;
    snakeDirection = "down";

    for (var index = snakeLength - 1; index >= 0; index--) {
        snake.push({
            x: index,
            y: 0
        });
    }
}

function snakeDraw() {
    for (var index = 0; index < snake.length; index++) {
        
        context.strokeStyle="yellow";
        context.strokeRect(snake[index].x * snakeSize, snake[index].y * snakeSize, snakeSize, snakeSize);
        context.lineWidth= 7;
        
        
        context.drawImage(image2, snake[index].x * snakeSize, snake[index].y * snakeSize, snakeSize, snakeSize);
    }
}

function snakeUpdate() {
    var snakeHeadX = snake[0].x;
    var snakeHeadY = snake[0].y;

    if (snakeDirection == "down") {
        snakeHeadY++;
    }
    else if (snakeDirection == "right") {
        snakeHeadX++;
    }
    else if (snakeDirection == "up") {
        snakeHeadY--;
    }
    else if (snakeDirection == "left") {
        snakeHeadX--;
    }

    checkFoodCollisions(snakeHeadX, snakeHeadY);
    checkWallCollisions(snakeHeadX, snakeHeadY);
    checkSnakeCollisions(snakeHeadX, snakeHeadY);
    var snakeTail = snake.pop();
    snakeTail.x = snakeHeadX;
    snakeTail.y = snakeHeadY;
    snake.unshift(snakeTail);
}


/*-----------------------------------------------------------------------------
 * Food Functions
 * ----------------------------------------------------------------------------
 */

function foodInitialize() {
    food = {
        x: 0,
        y: 0

    };
    setFoodPosition();
}


function foodDraw() {
    context.drawImage(image, food.x * snakeSize, food.y * snakeSize, 30, 30);
}

function setFoodPosition() {
    var randomX = Math.floor(Math.random() * screenWidth);
    var randomY = Math.floor(Math.random() * screenHeight);

    food.x = Math.floor(randomX / snakeSize);
    food.y = Math.floor(randomY / snakeSize);
}


/*-----------------------------------------------------------------------------
 * Input Functions
 * ----------------------------------------------------------------------------
 */

function keyboardHandler(event) {
    console.log(event);

    if (event.keyCode == "39" && snakeDirection != "left") {
        snakeDirection = "right";
    }
    else if (event.keyCode == "40" && snakeDirection != "up") {
        snakeDirection = "down";
    }
    else if (event.keyCode == "38" && snakeDirection !== "down") {
        snakeDirection = "up";
    }

    else if (event.keyCode == "37" && snakeDirection !== "right") {
        snakeDirection = "left";
    }
}
/*-----------------------------------------------------------------------
 * Collision Handling
 * -----------------------------------------------------------------------
 */

function checkFoodCollisions(snakeHeadX, snakeHeadY) {
    if (snakeHeadX == food.x && snakeHeadY == food.y) {
        snake.push({
            x: 0,
            y: 0
        });
        snakeLength++;
        foodDraw();
        setFoodPosition();
        blopSound.play();
                


    }
}

function checkWallCollisions(snakeHeadX, snakeHeadY) {
    if (snakeHeadX * snakeSize >= screenWidth || snakeHeadX * snakeSize < 0) {
        setState("GAME OVER");
        deathSound.play();
    }
    else if (snakeHeadY * snakeSize >= screenHeight || snakeHeadY * snakeSize < 0) {
        setState("GAME OVER");
        deathSound.play();
    }
    
    
    
}


function checkSnakeCollisions(snakeHeadX, snakeHeadY) {
    for(var index = 1; index < snake.length; index++) {
        if(snakeHeadX === snake[index].x && snakeHeadY === snake[index].y) {
            setState("GAME OVER");
            return;
            deathSound.play();
        }
    }
}
/*-----------------------------------------------------------------------------
 * Game State Handling
 * ----------------------------------------------------------------------------
 */

function setState(state) {
    gameState = state;
    showMenu(state);
}
/*-----------------------------------------------------------------------------
 * Menu Functions
 * ----------------------------------------------------------------------------
 */



function displayMenu(menu) {
    menu.style.visibility = "visible";
}

function hideMenu(menu) {
   menu.style.visibility = "hidden"; 
}

function showMenu(state) {
    if(state == "GAME OVER") {
        displayMenu(gameOverMenu);
    }
    else if(state == "PLAY") {
       displayMenu(playHUD);
    }
}

function centerMenuPosition(menu){
    menu.style.top = (screenHeight / 2) - (menu.offsetHeight / 2) + "px";
    menu.style.left = (screenWidth / 2) - (menu.offsetWidth / 2) + "px";
}

function drawScoreboard() {
    scoreboard.innerHTML = "Score: " + snakeLength;
    Highscore.innerHTML = "HighScore: " + localStorage.getItem(HighscoreX);
}

function HighScore() {
    if(localStorage.getItem(HighscoreX) < snakeLength) {
       localStorage.setItem(HighscoreX , snakeLength);
    }
}




